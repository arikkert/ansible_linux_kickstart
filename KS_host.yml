#
# pre:
#   host is installed with RedHat (compatible) 8 or higher
#
# post:
#   host is configured for kickstart with sources on FTP, HTTP(S) and/or NFS.
#

---
- name: Deploy kickstart host
  hosts: kshost
  become: true
  vars:
    _ansible_os_family: "RedHat"
    _ansible_distribution_major_version: 8
  vars_files: constants/constants.yml
  tasks:

    #
    # common stuff
    #

    - name: Check target OS
      ansible.builtin.assert:
        that:
          - ansible_os_family == _ansible_os_family
          - ansible_distribution_major_version | int >= _ansible_distribution_major_version
        fail_msg: "Playbook only suitable for distro {{ _ansible_os_family }} major version >= {{ _ansible_distribution_major_version }}"
        success_msg: "Yes, OS is fine"

    # disable SELinux cause I expect some complexity with
    # loopback mounting the iso's over NFS and sharing this content
    # over FTP, HTTP and NFS

    - name: Disable SELinux
      ansible.posix.selinux:
        policy: targeted
        state: permissive

    - name: Ensure firewalld is installed
      ansible.builtin.package:
        name: firewalld

    - name: Ensure FW is running
      ansible.builtin.systemd:
        name: firewalld
        state: started
        enabled: true

    - name: Set timezone
      community.general.timezone:
        name: "{{ timezone }}"

    #
    # TFTP
    #

    - name: Ensure required packages for TFTP are installed
      ansible.builtin.dnf:
        name:
          - tftp-server
          # - syslinux
          - syslinux-tftpboot

    - name: Ensure the syslinux files are in the correct location
      ansible.builtin.copy:
        src: /tftpboot/
        dest: "{{ tftpdir }}"
        remote_src: true
        mode: preserve

    - name: Ensure the syslinux files are in the correct location
      ansible.builtin.copy:
        src: /tftpboot/
        dest: "{{ tftpdir }}"
        remote_src: true
        mode: preserve

    - name: Set selinux context for {{ tftpdir }}
      ansible.builtin.command: restorecon -rv {{ tftpdir }}
      changed_when: false

    - name: Ensure tftp is installed via systemd
      ansible.builtin.include_tasks: KS_host_tftp_systemd.yml

    - name: Ensure tftp is installed via xinetd
      ansible.builtin.include_tasks: KS_host_tftp_xinet.yml
      when: false  # prefer systemd

    - name: Ensure FW is open for tftp
      ansible.posix.firewalld:
        service: "{{ item }}"
        state: enabled
        immediate: true
        permanent: true
      loop:
        - tftp

    - name: Ensure debug packages are installed
      ansible.builtin.dnf:
        name:
          - tftp

    #
    # FTP
    #

    - name: Ensure packages for sharing via FTP are installed
      ansible.builtin.dnf:
        name: vsftpd

    - name: Ensure vsftpd.conf is saved
      ansible.builtin.command: cp -p /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.conf.org
      args:
        creates: /etc/vsftpd/vsftpd.conf.org

    - name: Ensure vsftpd is configured anon_root
      ansible.builtin.lineinfile:
        path: /etc/vsftpd/vsftpd.conf
        line: anon_root={{ distdir }}
      notify: Handler restart vsftpd

    - name: Ensure vsftpd is configured
      ansible.builtin.lineinfile:
        path: /etc/vsftpd/vsftpd.conf
        regexp: '^anonymous_enable='
        line: anonymous_enable=YES
      notify: Handler restart vsftpd

    - name: Ensure vsftpd is running
      ansible.builtin.systemd:
        name: vsftpd
        state: started
        enabled: true

    - name: Ensure FW is open for ftp
      ansible.posix.firewalld:
        service: "{{ item }}"
        state: enabled
        immediate: true
        permanent: true
      loop:
        - ftp

    - name: Ensure debug packages are installed
      ansible.builtin.dnf:
        name:
          - ftp
          - lftp

    #
    # HTTP
    #

    - name: Ensure packages for sharing via HTTP are installed
      ansible.builtin.dnf:
        name:
          - httpd
          - mod_ssl
          - python3-cryptography  # needed on target server for openssl ansible modules

    - name: Ensure private key is created
      community.crypto.openssl_privatekey:
        path: /etc/httpd/self.key
        size: 2048
      tags: ssl

    - name: Ensure csr is created
      community.crypto.openssl_csr:
        path: /etc/httpd/self.csr
        privatekey_path: /etc/httpd/self.key
      tags: ssl

    - name: Ensure selfsigned cert is created
      community.crypto.x509_certificate:
        provider: selfsigned
        path: /etc/httpd/self.crt
        privatekey_path: /etc/httpd/self.key
        csr_path: /etc/httpd/self.csr
      tags: ssl

    - name: Ensure apache vhost is configured part 1
      ansible.builtin.copy:
        src: "{{ item }}"
        dest: /etc/httpd/conf.d/
        mode: '0644'
        backup: true
      notify: Handler reload apache
      loop:
        - welcome.conf

    - name: Ensure apache vhost is configured part 2
      ansible.builtin.template:
        src: "{{ item }}.j2"
        dest: /etc/httpd/conf.d/{{ item }}
        mode: '0644'
        backup: true
      notify: Handler reload apache
      loop:
        - ks.conf

    - name: Ensure apache is running
      ansible.builtin.systemd:
        name: httpd
        state: started
        enabled: true

    - name: Ensure FW is open for http
      ansible.posix.firewalld:
        service: "{{ item }}"
        state: enabled
        immediate: true
        permanent: true
      loop:
        - http
        - https

    #
    # NFS
    #

    - name: Ensure packages for sharing via NFS are installed
      ansible.builtin.dnf:
        name: nfs-utils

    - name: NFS-server is configured
      ansible.builtin.copy:
        src: exports
        dest: /etc/
        mode: '0640'
        backup: true
      notify: Handler reload nfs-server

    - name: Ensure nfs-server is running
      ansible.builtin.systemd:
        name: nfs-server
        state: started
        enabled: true

    - name: Ensure FW is open for nfs
      ansible.posix.firewalld:
        service: "{{ item }}"
        state: enabled
        immediate: true
        permanent: true
      loop:
        - nfs
        - rpc-bind
        - mountd

    #
    # common stuff
    #

    - name: Ensure debug packages are installed for ksvalidator
      ansible.builtin.dnf:
        name:
          - pykickstart

    - name: Ensure KS validate util is present
      ansible.builtin.copy:
        src: doksvalidate
        dest: "{{ distdir }}/ks/"
        mode: '0755'

  handlers:
    - name: Handler restart vsftpd
      ansible.builtin.systemd:
        name: vsftpd
        state: restarted
    - name: Handler reload apache
      ansible.builtin.systemd:
        name: httpd
        state: reloaded
    - name: Handler reload nfs-server
      ansible.builtin.systemd:
        name: nfs-server
        state: reloaded

...

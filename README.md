ks
--

Playbooks to install and configure a PXE kickstart server on a RHEL8/9 compatible server

Requirements
------------

- Ansible provision server with make installed
- RHEL 8/9 compatible target server that will become the kickstart server *kshost*.
- DHCP server configured to use the *kshost*. If DHCP is already configured for normal use, you probably only have to add something like (tested with KEA DHCP server) :
  - next-server: < ip of the *kshost* >;
  - boot-file-name: "pxelinux.0";
- NFS server with ISO files that can be NFS mounted from the *kshost*

Install
-------

make

Refs
----

- https://www.golinuxcloud.com/configure-kickstart-pxe-boot-server-linux/ RHEL8/CentOS8
- https://linuxhint.com/tftp_server_centos8/

constants
---------

defined as vars in the playbook, but these are defaults of the tftp-server and ftp-server packages and should not be changed.
```yaml
tftpdir: /var/lib/tftpboot
distdir: /var/ftp/pub
osdir: /mnt/OS
```
As FTP, NFS and HTTP(S) use the same directory to share the distribution its referred to as distribution directory.

hostvars
--------

Example with ISO's containing the currently latest stable versions of the OS to be kickstarted. All demo ISO's are FOSS. Download links can be found on for example https://distrowatch.com/


```yaml
 ---
 hostname_fqdn: "{{ ansible_hostname }}.mydomain"
 timezone: Europe/Amsterdam

 # {{ ksguests[].name }} is the logical name for the distribution to kickstart.
 # All ISOs should be mountable on {{ isomount }}/{{ ksguest[].iso }}, in this case an NFS mount
 # and {{ ksguests[].pxebootdir }} is the location from the root of the extracted ISO file with the pxeboot info
 isomount: nfsserver01.mydomain:/share/to/OS
 ksguests:
   - name: sysrescue
     iso: SystemRescue/SystemRescue/11.03/systemrescue-11.03-amd64.iso
     pxebootdir: sysresccd/boot
   - name: almalinux95demo
     iso: Unix/Linux/RedHat/AlmaLinux/9.5/AlmaLinux-9.5-x86_64-dvd.iso
     pxebootdir: images/pxeboot
   - name: almalinux89demo
     iso: Unix/Linux/RedHat/AlmaLinux/8.9/AlmaLinux-8.9-x86_64-dvd.iso
     pxebootdir: images/pxeboot
   - name: centos79demo
     iso: Unix/Linux/RedHat/CentOS/7.9/CentOS-7-x86_64-Everything-2009.iso
     pxebootdir: images/pxeboot
   - name: centos610demo
     iso: Unix/Linux/RedHat/CentOS/6.10/CentOS-6.10-x86_64-bin-DVD1.iso
     pxebootdir: images/pxeboot
   - name: fedora41demo
     iso: Unix/Linux/RedHat/Fedora/41/Fedora-Server-dvd-x86_64-41-1.4.iso
     pxebootdir: images/pxeboot


 # {{ my_kickstart_files }}/files/default/default_{{ myksguests[].name }}.j2 is the entry in the TFTP default file
 # {{ my_kickstart_files }}/files/ks/{{ myksguests[].name }}-ks.cfg is the kickstart file
 my_kickstart_files: /path/to/my/kickstart/files
 myksguests:
   - name: myksguest01
     iso: path/to/myksguest01iso
     pxebootdir: path/to/pxebootdir_from_iso_root
 ...
```

in a minimal deployment for testing the *kshost* itself, the *ksguests* and *myksguests* variables can be an emtpy list.
```
 ksguests: []
 myksguests: []
```

So the private configuration is separated from the public. That way the generic part containing the public stuff can be stored
in a public repo while the private configuration can be maintained in a private repo.

How it works (network kickstart)
--------------------------------

1. kickstart client boots and PXE BIOS broadcasts DHCP request (DHCP)
2. DHCP server sends client IP address/subnet/nameserver info + the IP of the kickstart server with TFTP server (DHCP)
3. kickstart client requests kickstart server to provide kernel + boot image + kickstart file (TFTP)
4. kickstart clients boots received kernel with image and continues with what is configured in the received kickstart file (FTP/HTTP/HTTPS)/NFS)

How it works (this implementation)
----------------------------------

On the *kshost* a TFTP server, FTP server, HTTP(S) server and NFS server is installed and configured.
systemd is used for TFTP.
This is done in the *KS_host.yml* playbook.

In the second playbook *KS_guests* the *kshost* is prepared for the specific clients that can be kickstarted. 
The ISO files with the OS to install during kickstart are not locally uploaded.
Instead they will be loop-back mounted from the NFS server.
The idea is that there is a dedictated storage server containing the ISOs.
The ISO content becomes available in */var/ftp/pub* which is not only the root dir for FTP, but also configured as root dir for HTTP(S) and NFS.
After the ISO is loop-back mounted on */var/ftp/pub/{{ ksguests[].name}}.cdrom*
the pxeboot directory (as defined in hostvar *ksguests[].pxebootdir*) is copied to */var/lib/tftpboot/images/{{ ksguests[].name }}*. These files are needed for the tftp boot phase and symlinks to the ISO content did not work for TFTP during tests.
The rest of the OS data should be available from the ISO itself, served via FTP, HTTP(S) and NFS. For the kickstart clients the ISO content is available with following URLs :
```
- ftp://{{ hostname_fqdn }}/{{ ksguests[].name }}
- http://{{ hostname_fqdn }}/{{ ksguests[].name }}
- https://{{ hostname_fqdn }}/{{ ksguests[].name }}
- nfs://{{ hostname_fqdn }}/var/ftp/pub/{{ ksguests[].name }}
```
So the content from the ISO that was mounted in from external NFS storage server is served by the *kshost* via NFS too. This requires *crossmnt* export option.

configuring the kickstart server
--------------------------------

These files are expected to be adapted by user:
- ansible hostvars of *kshost*
- *files/default/default_{{ ksguests[].name }}.j2*
- *templates/ks/{{ ksguests[].name }}-ks.cfg* files
- *{{ my_kickstart_files }}/files/default/default_{{ myksguests[].name }}.j2*
- *{{ my_kickstart_files }}/templates/ks/{{ myksguests[].name }}-ks.cfg*

1. Read the docs of the target OS to check which files are needed in the pxebootdir in the TFTP phase and that they are located in *{{ ksguests[].pxebootdir }}*.
2. Then create a logical name, determine the path to the ISO on the external NFS share and the pxebootdir.
3. Create a file *{{ my_kickstart_files }}/default/default_{{  ksguests[].name }}.j2* containing 1 entry of the *templates/default.j2* which will be generated dynamically during deploy as a concat of the individual entries. So its a 2 stage process: generate *default.j2* file, deploy *default.j2* on the target host.
4. Create a file *{{ my_kickstart_files }}/templates/ks/ksguests[].name }}-ks.cfg* with detailed configuration of the server to kickstarted.
Entries in the generated *templates/default.j2* can/should point to that kickstart file for the bootstage right after TFTP.

In the demo the kickstart files have a minimal configuration to serve an interactive install.
Of course they can be adapted to have a complete unattended batch install.

For testing purposes the *ksguests* var can be initialized as empty list *[]*.
The DOS Image and Memtest86+ Image should be able to boot, they do not need any ISO. It will boot in RAM and will not install anything on the guest disk.


demo gif
--------
demo of kickstarting fedora 36 as VMware client, attended (interactive) install.
- host_vars as in this README.
- empty myksguests.

![demo of kickstart session how it looks like in the console](demo.gif)

demo was created with vokoscreen and ffmpeg

YAMLLINT=@if which yamllint > /dev/null; then yamllint $@.yml; fi
ANSIBLELINT=@if which ansible-lint > /dev/null; then ansible-lint -q $@.yml; fi
OPTIONS=--diff #--check
PLAYBOOK=ansible-playbook $(OPTIONS) $@.yml

all: KS_host KS_guests

requirements:
	$(YAMLLINT)
	$(ANSIBLELINT)
	ansible-galaxy install -r $@.yml

KS_host KS_guests: requirements
	$(YAMLLINT)
	$(ANSIBLELINT)
	$(PLAYBOOK)
